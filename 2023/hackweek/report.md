---
title: Hackweek Report - 2023
subtitle: Hackweek 2023
institute: Tor Project - https://torproject.org
author: Silvio Rhatto
date: 2023-11-15
slides:
    aspect-ratio: 169
    font-size: 11pt
    table-of-contents: false

---

# Summary

Projects:

* [Onion Reveal coding and documenting (#15)](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/15)
* [Onion MkDocs tryout (#13)](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/13)
* [Onion TeX Slim enhancements (#14)](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/14)

# Building blocks

* Improved during this Hackweek:
  * [Onion MkDocs](https://tpo.pages.torproject.net/web/onion-mkdocs/)
  * [Onion TeX Slim](https://gitlab.torproject.org/tpo/community/onion-tex-slim)
  * [Reveal.js Tor Theme](https://gitlab.torproject.org/tpo/web/revealjs_tor_theme/)
* Created during this Hackweek:
  * [Onion Reveal](https://tpo.pages.torproject.net/web/onion-reveal/)
  * [L10n for Markdown](https://gitlab.torproject.org/tpo/community/l10n-for-markdown/)

# Results

* [The Tor Project Policies](https://gitlab.torproject.org/tpo/community/policies)
* [Tor's Hackweek](https://tpo.pages.torproject.net/community/hackweek/)
* [Tor Wikipelago](https://rhatto.pages.torproject.net/wikipelago/)
* [Community Team Training Materials](https://tpo.pages.torproject.net/community/training/)
