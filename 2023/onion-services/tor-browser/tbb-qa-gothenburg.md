---
title: Tor Browser Quality Assurance for Onion Services
subtitle: Gothenburg internal meeting
date: 2023-10-25
institute: Tor Project - https://torproject.org
author:
  - name: Silvio Rhatto
slides:
    #aspect-ratio: 169
    font-size: 11pt
    table-of-contents: false

---

# Overview

1. About the TBB QA for Onion Services.
2. Existing UX issues with HTTPS Certificates and Onion Services.
3. Current options.
4. Suggestions.

# Introduction

## TBB QA for Onion Services

* The TBB .onion QA [started][] informally in the beginning of 2023, when we
  spotted some issues with self-signed certificates.

* It soon was included in the [Sponsor145 2023.Q3 - 2024.Q2
  contract](https://nc.torproject.net/f/478798) with the following
  deliverable:

  > Test [...] onion services with new versions of Tor Browser: Tor will
  > continue to ensure that existing onion service sites work well with Tor
  > Browser with each new version.

## Sponsored work

* Up to 320 hours (2 working months) from the Onion Services SRE were allocated
  into this activity.

* The deliverable is not a specific list of features or bug fixes to be
  included in new Tor Browser releases.

* Instead, **it's about checking that Tor Browser is working as expected with
  Onion Services, and reporting otherwise**.

[started]: https://gitlab.torproject.org/tpo/onion-services/onionplan/-/issues/8

## Status

During the 2023.Q1 - 2023.Q3 period:

* 11 Tor Browser versions were formally tested.
* 8 security issues raised, mostly related with self-signed certificate handling.
* 1 documentation issue was filled along with a suggested fix.
* A [Lengthy Report][] written about (self-signed) certificates, Onion Services
  and the Tor Browser - 2023.Q3.

[Lengthy Report]: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41778#note_2938110

## Upcoming work

* Improving the test cases and procedures.

* Continue bootstrapping the "Faulty Onions", intended to provide test Onion
  Services with different errors to check how Tor Browser handles them.

* Try to automate some tests.

# Onion Services and self-signed certificates

# Summary by Example

## Enterprise Onion Impersonation Toolkit

Fake Tor onionsite using EOTK, vanity address and self-signed wildcard certificate
as shown by Tor Browser 13.0:

\includegraphics[width=0.6\textwidth]{images/00-onionsite.png}

Official Tor onionsite:

\includegraphics[width=0.6\textwidth]{images/06-official.png}

## Connection status

Fake Tor onionsite:

\includegraphics[width=0.6\textwidth]{images/02-connection-is-not-secure.png}

Official Tor onionsite:

\includegraphics[width=0.6\textwidth]{images/07-secure.png}

## Security details

Fake Tor onionsite:

\includegraphics[width=0.5\textwidth]{images/03-connection-details.png}

Official Tor onionsite:

\includegraphics[width=0.5\textwidth]{images/08-details.png}

## Identity

Fake Tor onionsite:

\includegraphics[width=0.6\textwidth]{images/04-website-identity.png}

Official Tor onionsite:

\includegraphics[width=0.5\textwidth]{images/09-website-identity.png}

## Certificate

\includegraphics[width=0.6\textwidth]{images/05-certificate.png}

## Problem statement

Right now, UI needs to handle an ambiguity:

1. Connection **may be considered secure** at the Onion Services level (with
   either HTTP or HTTPS).
2. Connection **may not be considered entirely secure** at the HTTPS level with
   self-signed certs, depending on the _authenticity_ property.

Having just a single behavior for multiple situations (HTTPS with CA-validated
or self-signed certificates) hides the ambiguity, and may lead to different
assumptions about the connection security.

# The Lengthy Report

## Historical background

* Massive migration from HTTP to HTTPS in the last 10-15 years.
* Onionsites were pushed into this direction, regardless of the security
  already provided by Onion Services, as web standards are relying
  more and more on HTTPS and on domain ownership authentication.
* It's taking a while for widespread CA-based HTTPS certs for .onions.
* And there's an inherent tension between the CA-world (hierarchical) and
  the distributed nature of Onion Services.
* Usage of HTTPS certificates with Onion Services are limited by these
  kinds of contingencies.

## Key points

1. The [current self-signed certificate on .onion behavior][] is confirmed to
   exist _by design and choice_.
2. **The site impersonation attack is way cheaper and easier to go unnoticed
   than with "regular" websites, which make it more worrying than usual.**
3. If this is a security issue, it's publicly known and is out there for around
   five years.
4. In __terms of classic information security aspects__: no big
   difference between self-signed certificates and free-of-charge and automated
   DV certificates for Onion Services. **The bigger difference is between
   self-signed/DV certs and EV certs.**; **if Onion Services start to be
   dependent on HTTPS, Certificate Authorities would have some control on which
   address are valid**.
5. It's recommended that [security icon specs][], labels and documentation
   should be reviewed and updated, as a lot was changed since the last
   iteration.
6. At the same time, changing behavior on client and library software may break
   standards and be hard to maintain.

[current self-signed certificate on .onion behavior]: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41778
[security icon specs]: https://support.torproject.org/onionservices/onionservices-5/

## Questions

1. How much Tor Browser would want to increase the reliance on the CA-model,
   and in which sense this question is a priority?
2. How important is to mitigate the impersonator attack vector against Onion
   Services by using CA-validated certificates or discouraging self-signed
   certificates? Are other mitigations more appropriate?
3. Would an enhancement on the [security icon specs][], labels, documentation
   and user awareness be enough to mitigate these attacks?

# Solutions

## General handling

Current _mutually exclusive_ options for _general handling_ (details [here][]):

| Option | Pros | Cons |
|--------|------|------|
| Error/warning for any self-signed .onion cert | Consistency, no ambiguities | Break existing setups, No alternative to CAs |
| [SOOC][]-like behavior | Alternative for the CA-model, compatible with existing [specs][], would not break existing setups | Ambiguous, more prone to impersonation |
| User option (button/setting) | No need to decide | Choosing the default, could break existing setups, difficult to understand |

[here]: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41778#note_2946295

## UI indicators

Current _complementary_ options for _UI indicators_ (details [here][]).
They can be implemented together, and both are compatible with either of the
above choices for general certificate handling:

| Option | Pros | Cons |
|--------|------|------|
| Additional icon in the [specs][] | Solves ambiguity, support self-signed or [SOOC][] icon | Many icons, harder to understand, departs from semaphore protocol |
| Labelling/disclaimer below the basic information as [described][] | Solves ambiguity, expanded explanation on feats. and expectations | Only works if users clicks, too much for mobile |

[specs]: https://support.torproject.org/onionservices/onionservices-5/
[SOOC]: https://tpo.pages.torproject.net/onion-services/onionplan/proposals/usability/certificates/#same-origin-onion-certificates-sooc
[described]: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41778#note_2945210

## Better documentation

Improving docs is always good!

We're working on this at [tpo/community/support#40121][].

[tpo/community/support#40121]: https://gitlab.torproject.org/tpo/community/support/-/issues/40121

# Conclusion

Suggestions:

1. **Discuss the opened issues and the overall strategy regarding Onion Services
   and self-signed certificates**.

2. **Mitigate somehow the site impersonation based on rewriting proxies and
   self-signed certs**.

3. **General revision in how UI handles certificates, and certificate
   information, to reduce inconsistencies**.
