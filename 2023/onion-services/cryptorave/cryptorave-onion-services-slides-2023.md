---
title: Os Serviços Cebola da Rede Tor
subtitle: CryptoRave 2023
date: 06/05/2023
institute: Projeto Tor - https://torproject.org/pt-BR
author:
  - name: Silvio Rhatto
slides:
    aspect-ratio: 169
    font-size: 11pt
    table-of-contents: false

---

# Início

## Roteiro

1. Introdução ao Tor e aos Serviços Cebola.
2. Pedir pro pessoal instalar o Tor Browser para Desktop e Android (https://www.torproject.org/pt-BR/download/) e/ou o Onion
   Browser para iOS (https://onionbrowser.com/) durante a apresentação.
3. Demonstração dos Serviços Cebola:
    1. Acessando sites.
    2. OnionShare.
    3. Onion Desktop.

## Resumo

Introdução aos Serviços Cebola (Onion Services) da [Rede
Tor](https://www.torproject.org/pt-BR/): o que são, como funcionam, para quê
servem, como usar, estado atual da tecnologia e perspectivas de
aperfeiçoamento.

## Objetivos

Nesta atividade, a tecnologia dos Serviços Cebola será apresentada,
convidando participantes a usá-la no dia-a-dia, assim como perspectivas de
desenvolvimentos futuros serão abordadas.

## Requisitos

Esta atividade começará pelo básico e prosseguirá o quanto possível :)

# O Tor

## O que é o Tor e como ele funciona

* Vide slides "Introdução aos Serviços Cebola".

# Serviços Cebola

## Recapitulando

Agora voltaremos ao tema dos Serviços Cebola, mas com nova ênfase.

## Demonstração

1. Acessando Serviços Cebola:
    * Desktop e Android: https://www.torproject.org/pt-BR/download/
    * iOS: https://onionbrowser.com/
2. OnionShare: https://onionshare.org
3. Onion Desktop: https://github.com/scanlime/onion-desktop

## Revisitando: o que são?

Os Serviços Cebola (Onion Services) são uma tecnologia de comunicação com
diversas propriedades fundamentais: criptografia de ponta a ponta,
resistência à censura e à vigilância, são portáteis, sem autoridade central
de gestão, número de endereços (\~$2^{256}$) maior que a quantidade de átomos na
galáxia (\~$2^{228}$) e com anonimato por padrão na ponta do cliente e
opcionalmente na do servidor!

## Revisitando: para quê servem?

Com os Serviços Cebola é possível enviar e receber arquivos, trocar mensagens
instantâneas, hospedar websites e muito mais, sem necessitar de
provedores de conteúdo intermediários!

## Revisitando: funcionamento

Os Serviços Cebola são acessíveis através da Rede Tor, uma tecnologia de
especial de roteamento que encapsula mensagens em camadas criptográficas que
garantem sigilo e autenticidade no fluxo de informações, e através de
softwares como o Tor Browser (https://www.torproject.org/pt-BR/download/),
OnionShare (https://onionshare.org/) e Quiet (https://tryquiet.org).

## Endereçamento

* Serviços Cebola são identificados por sua chave criptográfica pública.

* Atualmente não existe um espaço global de nomes facilmente memorizáveis, como
  no caso dos nomes de domínio (`example.org` etc).

* Seria como números de telefone, que são uma espécie de "endereço" de um
  aparelho/pessoa, mas com a diferença que o "número cebola" é a chave pública
  do endereço. E você não precisa de nenhuma autoridade como uma operadora de
  telefonia ou provedor de internet para ter seu próprio endereço: basta usar um
  computador para gerá-lo!

## Melhorias

* Uma das melhorias que estão sendo consideradas para a tecnologia é a
  compatibilização com endereços mais amigáveis e fáceis de memorizar.

## Dúvidas e contato

rhatto@torproject.org
