#
# Presentations Makefile.
#

# Relative folder pointing to where vendorized software is installed
#L10N_FOR_MARKDOWN_PATH=vendors/l10n-for-markdown
#ONION_MKDOCS_PATH   = vendors/onion-mkdocs
ONION_REVEAL_PATH   = vendors/onion-reveal
ONION_TEX_SLIM_PATH = vendors/onion-tex-slim

# Include additional Makefiles
# See https://www.gnu.org/software/make/manual/html_node/Include.html
#-include vendors/onion-mkdocs/Makefile.onion-mkdocs
-include vendors/onion-reveal/Makefile.onion-reveal
-include vendors/onion-tex-slim/Makefile.onion-tex-slim
-include vendors/onion-reveal/Makefile.http-server
